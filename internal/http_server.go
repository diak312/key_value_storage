package bootstrap

import (
	"apiService/controllers"
	"apiService/repositories"

	"net/http"
	"sync"

	"github.com/go-chi/chi/v5"
	"github.com/sirupsen/logrus"
)

type HttpServer struct {
}

func (httpServer *HttpServer) Init() error {
	repository := repositories.ValueRealisationRepository{Storage: sync.Map{}}
	controller := controllers.StorageController{&repository}

	r := chi.NewRouter()

	r.With(SecurityMiddleware).Get("/ping", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("pong"))
	})

	r.With(SecurityMiddleware).Get("/Get", func(w http.ResponseWriter, r *http.Request) {
		value, err := controller.GetValue(r.Context(), r.FormValue("key"))
		if err != nil {
			w.WriteHeader(500)
			w.Write([]byte(err.Error()))
			logrus.Errorf("Error '/Get' request ", err)
		} else {
			w.WriteHeader(200)
			w.Write([]byte(value))
			logrus.Info("Get request")
		}
	})

	r.With(SecurityMiddleware).Get("/List", func(w http.ResponseWriter, r *http.Request) {
		list, err := controller.GetList(r.Context())
		if err != nil {
			w.WriteHeader(500)
			w.Write([]byte(err.Error()))
			logrus.Errorf("Error '/List' request ", err)
		} else {
			w.WriteHeader(200)
			w.Write([]byte(list))
			logrus.Info("List request")
		}
	})

	r.With(SecurityMiddleware).Post("/Upsert", func(w http.ResponseWriter, r *http.Request) {
		err := controller.Upsert(r.Context(), r.FormValue("key"), r.FormValue("value"))
		if err != nil {
			w.WriteHeader(500)
			w.Write([]byte(err.Error()))
			logrus.Errorf("Error '/Post' request ", err)
		} else {
			w.WriteHeader(200)
			w.Write([]byte("Value added!"))
			logrus.Info("Upsert request")
		}
	})

	r.With(SecurityMiddleware).Delete("/Delete", func(w http.ResponseWriter, r *http.Request) {
		err := controller.Delete(r.Context(), r.FormValue("key"))
		if err != nil {
			w.WriteHeader(500)
			w.Write([]byte(err.Error()))
			logrus.Errorf("Error '/Delete' request ", err)
		} else {
			w.WriteHeader(200)
			w.Write([]byte("Value deleted!"))
			logrus.Info("Delete request")
		}
	})

	http.ListenAndServe(":80", r)

	return nil
}

func SecurityMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		//Выполняем проверку входящего запроса, лучше согласовать как, поэтому просто прокидываю дальше

		next.ServeHTTP(w, r)
	})
}
