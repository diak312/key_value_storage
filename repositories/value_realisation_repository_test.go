package repositories

import (
	"context"
	"sync"
	"testing"
)

func TestGetValue(t *testing.T) {
	repository, context := gettingStartingEntities()

	value, err := repository.GetValue(context, "1")

	if value != "test1" && err != nil {
		t.Error("Getting value test failed.")
	}
}

func TestGetList(t *testing.T) {
	repository, context := gettingStartingEntities()

	list, err := repository.GetList(context)

	if len(list) != 3 && err != nil {
		t.Error("Getting list values test failed.")
	}
}

func TestUpsert(t *testing.T) {
	repository, context := gettingStartingEntities()

	err := repository.Upsert(context, "4", "test4")

	value, ok := repository.Storage.Load("4")
	if value != "test4" && ok && err != nil {
		t.Error("Upsert value test failed.")
	}
}

func TestDelete(t *testing.T) {
	repository, context := gettingStartingEntities()

	err := repository.Delete(context, "1")

	value, ok := repository.Storage.Load("1")
	if value != "" && ok && err != nil {
		t.Error("Deletion value test failed.")
	}
}

func gettingStartingEntities() (ValueRealisationRepository, context.Context) {
	repository := ValueRealisationRepository{Storage: sync.Map{}}

	repository.Storage.Store("1", "test1")
	repository.Storage.Store("2", "test2")
	repository.Storage.Store("ggg15", "test3")

	return repository, context.Background()
}
