package repositories

import "context"

type ValueRepository interface {
	GetValue(ctx context.Context, key string) (string, error)
	GetList(ctx context.Context) (map[string]string, error)
	Upsert(ctx context.Context, key string, value string) error
	Delete(ctx context.Context, key string) error
}
