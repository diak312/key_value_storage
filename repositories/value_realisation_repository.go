package repositories

import (
	"context"
	"errors"
	"fmt"
	"sync"
)

type ValueRealisationRepository struct {
	Storage sync.Map
}

func (repository *ValueRealisationRepository) GetValue(ctx context.Context, key string) (string, error) {
	value, ok := repository.Storage.Load(key)
	if !ok {
		return "", errors.New("Key not found!")
	}

	return value.(string), nil
}

func (repository *ValueRealisationRepository) GetList(ctx context.Context) (map[string]string, error) {
	list := map[string]string{}
	repository.Storage.Range(func(key, value interface{}) bool {
		list[fmt.Sprint(key)] = value.(string)
		return true
	})
	if len(list) == 0 {
		return nil, errors.New("Storage is empty!")
	}

	return list, nil
}

func (repository *ValueRealisationRepository) Upsert(ctx context.Context, key string, value string) error {
	repository.Storage.Store(key, value)
	return nil
}

func (repository *ValueRealisationRepository) Delete(ctx context.Context, key string) error {
	repository.Storage.Delete(key)
	return nil
}
