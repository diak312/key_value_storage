package main

import (
	"fmt"

	bootstrap "apiService/internal"
)

func main() {
	httpServer := bootstrap.HttpServer{}

	err := httpServer.Init()

	if err != nil {
		fmt.Println(err)
	}
}
