package controllers

import (
	"apiService/repositories"
	"context"

	"bytes"
	"fmt"
)

type StorageController struct {
	/* Сюда поидее еще хорошо бы вставить сервис по обработке входящих данных,
	если вдруг появится взаимодействие с апи или каким-то другим источником данных
	но при такой задаче мне кажется это избыточно */

	StorageRepository repositories.ValueRepository
}

func (controller *StorageController) GetValue(ctx context.Context, key string) (string, error) {
	value, err := controller.StorageRepository.GetValue(ctx, key)
	if err != nil {
		return "", err
	}

	return value, nil
}

func (controller *StorageController) GetList(ctx context.Context) (string, error) {
	b := new(bytes.Buffer)

	keyValueMap, err := controller.StorageRepository.GetList(ctx)
	if err != nil {
		return "", err
	}

	for key, value := range keyValueMap {
		fmt.Fprintf(b, "%s=\"%s\"\n", key, value)
	}

	keyValueList := b.String()

	return keyValueList, nil
}

func (controller *StorageController) Upsert(ctx context.Context, key string, value string) error {
	err := controller.StorageRepository.Upsert(ctx, key, value)
	if err != nil {
		return err
	}

	return nil
}

func (controller *StorageController) Delete(ctx context.Context, key string) error {
	err := controller.StorageRepository.Delete(ctx, key)
	if err != nil {
		return err
	}

	return nil
}
